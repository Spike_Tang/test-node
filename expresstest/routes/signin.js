var express = require('express')
var router = express.Router()

var UserModel = require('../schema/user')

var checkLogin = require('../middlewares/check').checkLogin

router.post('/', checkLogin, function(req, res, next) {
  var name = req.body.account
  var password = req.body.password

  UserModel.getUserByName(name).then(function(result) {
    //成功返回
    console.log('result', result)
    var data = result.result
    if (!data) {
      return res.send('用户不存在')
    }
    if (password !== data.userpwd) {
      return res.send('用户名或密码错误')
    }
    delete data.userpwd
    req.session.user = data
    res.send('登录成功')
  }, function(res) {
    //失败返回
    console.log(res)
    res.send('查询失败')
  })
})

module.exports = router