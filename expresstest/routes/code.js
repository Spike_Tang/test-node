var express = require('express')
var router = express.Router()

var CodeModel = require('../schema/codedata')

var checkLogin = require('../middlewares/check').checkLogin

router.post('/save', function(req, res, next){
  console.log(req.body)
  console.log(req.query)
  console.log(req.params)
  CodeModel.saveCode(req.body).then(function(result){
    res.send(result)
  },function(err){
    res.send(err)
  })
})

router.post('/find', function(req, res, next){
  console.log(req.body)
  console.log(req.query)
  console.log(req.params)

  CodeModel.findCode().then(function(result){
    res.send(result)
  },function(err){
    res.send(err)
  })
})


module.exports = router