const cheack = require('../middlewares/check.js').checkLogin

module.exports = function(app) {
  app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "X-Requested-With,Content-Type")
    res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS")
    res.header("X-Powered-By",' 3.2.1')
    res.header("Content-Type", "application/json;charset=utf-8")
    next()
  })
  app.get('/', cheack, function(req, res) {
    //res.redirect('/home/home.html')
    // res.render('../public/home/home.html')
    res.send('home')
  })
  app.use('/code', require('./code'))
  // app.use('/list', require('./list'))
  // app.use('/signup', require('./signup'))
  // app.use('/signin', require('./signin'))
  // app.use('/signout', require('./signout'))
  // app.use('/posts', require('./posts'))

  app.use(function(req, res) {
    //res.render('../public/home/home.html')
    res.send('home')
  })
  // 404 page
  // app.use(function (req, res) {
  //   if (!res.headersSent) {
  //     res.status(404).render('404')
  //   }
  // })
}