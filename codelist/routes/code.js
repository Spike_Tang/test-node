var express = require('express')
var router = express.Router()

var multer  = require('multer')
var upload = multer({ dest: 'upload/' });

var CodeModel = require('../schema/codedata')

router.post('/save', function(req, res, next){
  CodeModel.saveCode(req.body).then(function(result){
    res.send('添加成功')
  },function(err){
    res.send(err)
  })
})

router.post('/uploads', upload.single('avatar'), function(req, res, next){
  console.log(res.file)
  console.log('成功')
  res.send('进入')
})

router.post('/upload', upload.single('logo'), function(req, res, next){
    var file = req.file;

    console.log('文件类型：%s', file.mimetype);
    console.log('原始文件名：%s', file.originalname);
    console.log('文件大小：%s', file.size);
    console.log('文件保存路径：%s', file.path);

    res.send({ret_code: '0'});
});

router.post('/find', function(req, res, next){
  console.log('查找')
  CodeModel.findCode().then(function(result){
    res.send(result)
  },function(err){
    res.send(err)
  })
})

router.post('/update', function(req, res, next){
  // console.log('body',req.body)
  var id = req.body.id;
  var data = req.body.data;
  CodeModel.updateCode(id,data).then(function(result){
    res.send('保存成功')
  },function(err){
    res.send(err)
  })
})

router.post('/delete', function(req, res, next){
  // console.log('body',req.body)
  var id = req.body.id;
  CodeModel.deleteCode(id).then(function(result){
    res.send('删除成功')
  },function(err){
    res.send(err)
  })
})

module.exports = router