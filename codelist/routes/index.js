const cheack = require('../middlewares/check.js').checkLogin

module.exports = function(app) {
  app.all('*', function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With , yourHeaderFeild');
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next()
  })
  var DIR = './uploads/';
  // 文件上传插件

  app.get('/', cheack, function(req, res) {
    res.send('home')
  })

  app.use('/code', require('./code'))
  // 404 page
  app.use(function (req, res) {
    res.send('404')
  })
}