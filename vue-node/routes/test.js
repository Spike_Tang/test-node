var express = require('express');
var router = express.Router();

const LoginMiddle = require('../middlewares/loginMiddle.js');
const Login = new LoginMiddle();
const checkLogin = Login.checkLogin;

router.get('/', checkLogin, (req, res, next) => {
  res.send('test');
});

module.exports = router;
