const express = require('express');
const router = express.Router();

const ShopCtrl = require('../controller/shopping/shopCtrl.js');
const Shop = new ShopCtrl();

router.post('/addshop', Shop.addShop);

module.exports = router;