
const users = require('./users');
const shop = require('./shopping');
const v1 = require('./v1');

const test = require('./test');

module.exports = (app) => {
  // app.use('/', index);
  app.use('/user', users);
  app.use('/shop', shop);
  app.use('/v1', v1);

  app.use('/test', test);
}