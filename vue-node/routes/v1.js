const express = require('express');
const router = express.Router();

const baseComponent = require('../prototype/baseComponent.js');
const baseHandle = new baseComponent();

router.post('/addimg/:type', baseHandle.uploadImg);

module.exports = router;


