var express = require('express');
var router = express.Router();

let UserCtrl = require('../controller/users.js');
let User = new UserCtrl();

router.post('/signup', User.signup);
router.post('/signin', User.signin);
router.get('/signout', User.signout);

module.exports = router;
