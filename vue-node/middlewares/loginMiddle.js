class LoginMiddle {
  constructor () {

  };

  checkLogin (req, res, next) {
    if (!req.session.user) {
      console.log('error', '未登录');
      //res.redirect('/login.html');
    } else {
      console.log('已经登陆', req.session)
    };
    next();
  };

  checkNotLogin (req, res, next) {
    if (req.session.user) {
      console.log('error', '已登录');
      res.redirect('back');//返回之前的页面
      return;
    }
    next();
  };
};

module.exports = LoginMiddle;