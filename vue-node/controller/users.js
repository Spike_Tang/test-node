const UserModel = require('../models/user-model.js');

class UserCtrl {
  constructor () {

  };

  async signup (req, res, next) {
    let data = {
      account: req.body.account,
      password: req.body.password
    };
    UserModel.create(data).then(function(res){
      console.log(res);
    });
    res.send('注册成功')
  };

  async signin (req, res, next) {
    let findData = {
      account: req.body.account
    };
    UserModel.findOne(findData).then(function(data){
      if (data.password == req.body.password) {
        delete data.password;
        req.session.user = data;
        res.send('密码正确');
      }else {
        res.send('密码错误');
      };
    });
  };

  async signout (req, res, next) {
    req.session.user = null;
    res.send('清空 session');
  };
}

module.exports = UserCtrl;