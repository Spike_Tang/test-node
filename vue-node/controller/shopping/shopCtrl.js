const shopModel = require('../../models/shopping/shop-model.js');

class shop {
  constructor () {

  };

  async addShop (req, res, next) {
    let data = req.body;
    try{
      if (!data.name) {
        throw new Error('必须填写名称');
      }else if (!data.phone) {
        throw new Error('必须填写电话');
      };
    }catch (err) {
      res.send({
        status: 0,
        message: err.message
      });
      return;
    };
    const newShop = {
      name: data.name,
      phone: data.phone
    };
    try{
      const shop = new shopModel(newShop);
      await shop.save();
      res.send({
        status: 1,
        sussess: '添加成功',
        shopDetail: newShop
      });
    }catch (err) {
      res.send({
        status: 0,
        message: '添加失败'
      });
    }
  };

};

module.exports = shop;