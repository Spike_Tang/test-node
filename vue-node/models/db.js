var mongoose = require('mongoose'),
  DB_URL = 'mongodb://root:327921510@localhost:27017/expresstest';

mongoose.Promise = global.Promise;
/**
 * 连接
 */
mongoose.connect(DB_URL);


mongoose.connection.once('open', () => {
	console.log('连接数据库成功')
})
/**
 * 连接成功
 */
mongoose.connection.on('connected', function() {
  console.log('Mongoose connection open to ' + DB_URL);
});

/**
 * 连接异常
 */
mongoose.connection.on('error', function(err) {
  console.log('Mongoose connection error: ' + err);
  mongoose.disconnect();
});

/**
 * 连接断开
 */
mongoose.connection.on('disconnected', function() {
  console.log('Mongoose connection disconnected');
});

mongoose.connection.on('close', function() {
    console.log('数据库断开，重新连接数据库');
    mongoose.connect(DB_URL, {server:{auto_reconnect:true}});
});

module.exports = mongoose;