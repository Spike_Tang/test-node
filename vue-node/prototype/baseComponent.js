const formidable = require('formidable');

class BaseComponent {
  constructor () {
    this.uploadImg = this.uploadImg.bind(this);
  };

  async uploadImg (req, res, next) {
    try{
      const image_path = await this.getPath(req);
      console.log(image_path)
      res.send({
        status: 1,
      });
    }catch (err) {
      console.log(err.message)
      res.send({
        status: 0,
        type: 'ERROR_UPLOAD_IMG',
        message: '上传图片失败'
      });
    };
  };

  async getPath (req) {
    return new Promise((resolve, reject) => {
      const form = new formidable.IncomingForm();
      form.uploadDir = './public/img';
      form.parse(req, async (err, fields, files) => {
        console.log(err)
        console.log(files)
        console.log(fields)
        // res.send('上传')
      });
    });
  };

};

module.exports = BaseComponent;